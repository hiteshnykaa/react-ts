import React, {Component} from 'react';
import './App.css';
import Header from './components/Header';
import Cart from './components/Cart'

class App extends Component{
  render = () => {
    return(
      <div className="layout">
        <Header />
      <div>
        <Cart />
      </div>
      </div>
    )
  }
}

export default App;

