import React, { Component, createRef } from 'react';

function toggleStateHandler ( toggleClass: any) {
    const node = toggleClass.cartRef.current
    
    if(node){
        node.style.display = "none"
    }
}


class Cart extends Component {      
    private cartRef = createRef<HTMLDivElement>()

    render = () => {         
        const style: React.CSSProperties ={
            backgroundColor: "gray",
            height: "100%",
            width: "30%",
            display:"block",
            zIndex: 2,
            position:"fixed",
            right: 0
        } 

    return (
        <div style={style} ref={this.cartRef}>
            <p style={{textAlign: "right", marginRight: "20px", marginTop:"20px", cursor:"pointer"}} 
            className="close"
            onClick = {()=>toggleStateHandler(this)}> X </p>
        </div>
        )
    }
}

export default Cart
export toggleStateHandler