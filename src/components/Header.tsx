import React from 'react';
import Cart from './Cart'
import './header.css'

const Header: React.FC = () => {

    return (
    <header className="header">
        <a className="image" href="/">
        <img className="logo" 
        src="https://adn-static1.nykaa.com/nykdesignstudio-images/pub/media/logo/stores/1/nykaa_logo.svg" />
        </a>
        <ul className="categories">
            <li> Category1 </li>
            <li> Category2 </li>
            <li> Category3 </li>
            <li> Category4 </li>
            <li> Category5 </li>
        </ul>

        <div className="cart" >
            <img className="showCart"
            src="https://cdn4.iconfinder.com/data/icons/shopping-21/64/shopping-01-512.png" />
        </div>

        
    </header>
    );
}

export default Header